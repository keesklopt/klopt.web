all: package

base_dir=/tmp/fpm
inst_dir=$(base_dir)/usr/share/klopt
conf_dir=$(base_dir)/etc/apache2/sites-available
web_dir=/var/www/html/repo
pack_dir=~/projects/packages
version=1.12

package:
	mkdir -p $(inst_dir)/web/html
	mkdir -p $(conf_dir)
	cp -r . $(inst_dir)/web/html
	cp web.conf $(conf_dir)
	rm -f *.deb
	fpm -s dir -t deb -n "klopt-web" \
		--after-install web_post.sh	\
		-d libapache2-mod-wsgi		\
		-v $(version)				\
		-C $(base_dir)  .
	rm -rf $(base_dir)

provision:
	cp *.deb $(pack_dir)

# su first !
install: 
	cp *.deb $(web_dir)/klopt
	cd $(web_dir); dpkg-scanpackages klopt /dev/null | gzip -9c > klopt/Packages.gz
