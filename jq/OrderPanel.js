function OrderPanel(name) {
	function init () { 
		self.panel = $(name)

		elm = self.panel.append("<b>Orders</b>");
		elm = self.panel.append("<ul>");
		for (r=0; r< 10; r++) { 
			elm.append('<li class="ui-state-default">' + r + '</li>');
		} 
    	elm.sortable({
      		placeholder: "ui-state-highlight"
		});
    	elm.disableSelection();
	} 
	init();
}
