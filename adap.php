<?
ini_set("include_path", ".:feyenoord");

session_start();

require_once "database.php";
require_once "mysql.php";
require_once "timepattern.php";
require_once "ajax.php";

$cmd=$_GET['cmd'];
$json=$_GET['json'];
if (!$json) {
// askbar uses POST
// feyenoord should also ?
$cmd=$_POST['cmd'];
$json=$_POST['json'];
}

function adaptime_error($inmsg,$err)
{
    $gm = new JsonResult("error", $inmsg->cmd, $err);
    return $gm;
}

function open_proxy_db($msg)
{
    global $PROXY_USER, $PROXY_NAME, $PROXY_HOST, $PROXY_PASS;
    $auth = new Auth($PROXY_USER,$PROXY_PASS);
    $mysql = new MySql($PROXY_HOST,$PROXY_NAME,$auth);
    if (!$mysql->con) {
        if (!$mysql->err) {
             $msg = error_message($mysql->error());
    } else 
        $msg = $mysql->err;
    }

    return $mysql;
}

function open_ivo_db()
{
    global $IVO_USER, $IVO_NAME, $IVO_HOST, $IVO_PASS;
    $auth = new Auth($IVO_USER,$IVO_PASS);
    $mysql = new MySql($IVO_HOST,$IVO_NAME,$auth);
    if (!$mysql->con) {
        if (!$mysql->err) echo $mysql->error();
        else echo "fout!";
    }

    return $mysql;
}

function make_tree($aa,$s)
{
    if ($s==NULL) $parent==NULL;
    else $parent=$s['id'];
    foreach ($aa as $sub) {
        $grp=$sub['parent'];
        if ($grp == $parent) {
            if ($tree == null)
                $tree = array();
            $k = $sub['key'];
            $tree[$k] = make_tree($aa,$sub);
        }
    }
    if ($tree==null) {
        return $s;
    }
    return $tree;
}

// for now, just get everything
function get_timepattern($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $id = $inmsg->get_object(0);

    $tp = new TimePattern(null,null);
    $tp->read($mysql,$id);
    $gm->add_object($tp);

    return $gm;
}

class budget
{
    function budget($userid,$id,$start,$end,$amount,$unit,$activitytype,$priority) 
    {
        $this->userid =$userid;
        $this->id=$id;
        $this->start =$start;
        $this->end =$end;
        $this->amount =$amount;
        $this->unit =$unit;
        $this->activitytypeid =$activitytype;
        $this->priority =$priority;
    }
}

// get all budgets base information
function get_budgets($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $id = $inmsg->get_object(0);
    $start = floor($inmsg->get_object(1)/1000);
    $end = floor($inmsg->get_object(2)/1000);

    // date-less select
    // construct all budgets having any overlap with the current week
    // split if needed (for instance DAILY etc)
    $qry = "SELECT * from userbudget as ub left join budgets as b on ub.budgetid=b.id where userid=$id AND b.start< $end AND b.end > $start ORDER BY priority";

    $res = $mysql->qry_assoc($qry);

    $restack = array();

    // select and split
    foreach ($res as $budget) {
        if ($budget['unit'] == "WEEKDAY") {
            for ($d =0; $d< 5; $d++) {
                $end = $start + 86400;
                $newbudget = new budget($budget['userid'], $budget['id'], $start,$end,$budget['amount'],$budget['unit'],$budget['activitytypeid'],$budget['priority']) ;
                $start= $end;
                $l =count($restack);
                $restack[$l] = $newbudget;
            }
        }
        if ($budget['unit'] == "DAY") {
            for ($d =0; $d< 7; $d++) {
                $end = $start + 86400;
                $newbudget = new budget($budget['userid'], $budget['id'], $start,$end,$budget['amount'],$budget['unit'],$budget['activitytypeid'],$budget['priority']) ;
                $start= $end;
                $l =count($restack);
                $restack[$l] = $newbudget;
            }
        }
        if ($budget['unit'] == "WEEK") {
                $end = $start + 86400*7;
                $newbudget = new budget($budget['userid'], $budget['id'], $start,$end,$budget['amount'],$budget['unit'],$budget['activitytypeid'],$budget['priority']) ;
                $l =count($restack);
                $restack[$l] = $newbudget;
        }
        // do two months if needed 
        if ($budget['unit'] == "MONTH") { 
                $end = $start + 86400*7;
                $newbudget = new budget($budget['userid'], $budget['id'], $start,$end,$budget['amount'],$budget['unit'],$budget['activitytypeid'],$budget['priority']) ;
                $l =count($restack);
                $restack[$l] = $newbudget;
        }
        if (! $budget['unit'] || $budget['unit'] == "" || $budget['unit'] == "NONE") {
                $newbudget = new budget($budget['userid'], $budget['id'], $budget['start'],$budget['end'],$budget['amount'],$budget['unit'],$budget['activitytypeid'],$budget['priority']) ;
                $l =count($restack);
                $restack[$l] = $newbudget;
        }
    }

    $gm->add_object($restack);
    $gm->add_object($res);

    return $gm;
}

// get all budgets base information
function get_allbudgets($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $qry = "SELECT * from budgets ORDER BY priority";

    $res = $mysql->qry_assoc($qry);
    $gm->add_object($res);

    return $gm;
}

// get the data of a particular budget
function get_userbudget($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $id = $inmsg->get_object(0);

    $qry = "SELECT * from userbudget where budgetid='" . $id . "'";
    $res = $mysql->qry_assoc($qry);
       $gm->add_object($res);

    return $gm;
}

// put new budget for user, delete old!
function set_userbudget($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $id = $inmsg->get_object(0);
    $budgets = $inmsg->get_object(1);

    // DELETE first
    $qry = "DELETE from userbudget where budgetid='" . $id . "'";
    $res = $mysql->qry_assoc($qry);

    // now reinsert
    $cnt=0;
    $qry = "INSERT INTO userbudget (budgetid,userid) VALUES ";
    foreach ($budgets as $uid) {
        if ($cnt != 0) $qry .= ",";
        $qry .= "(" . $id . "," . $uid . ")";
        $cnt++;
    }
    $res = $mysql->qry_boolean($qry);

       $gm->add_object($qry);

    return $gm;
}

function set_usersync($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $uid = $inmsg->get_object(0);
    $keyid = $inmsg->get_object(1);
    $value = $inmsg->get_object(2);

    // DELETE first
    $qry = "REPLACE INTO usersettings (`userid`,`key`,`value`) VALUES ('$uid','$keyid','$value')";
    $res = $mysql->qry_boolean($qry);

    return $gm;
}

function get_settings($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);


    $qry = "SELECT * FROM settings";
    $res = $mysql->qry_assoc($qry);

    // settings have an optional parent, 
    // making hierarchical settings possible
    $object = make_tree($res,NULL);

    if (!$res) {
        //logout();
        $gm = adaptime_error($inmsg,"get_settings");
    } else 
        $gm->add_object($object);
    return $gm;
}

function put_setting($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $key=$inmsg->get_object(0);
    $val=$inmsg->get_object(1);

    $qry = "UPDATE settings" .
          " SET `value`='" .  $val. 
          "' WHERE `key`= '" . $key . "'";

    $res = $mysql->qry_boolean($qry);
    // just reread !

    return get_settings($inmsg);
}

function clr_calendar($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $userid=$inmsg->get_object(0);
    $application=$inmsg->get_object(1);

	// first delete 
    $qry = "DELETE FROM calendars WHERE `userid`='" . $userid . 
		"' AND `origin`='" . $application ."'";
    $res = $mysql->qry_boolean($qry);

    return $gm;
}

function put_calendar($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);	

    $userid=$inmsg->get_object(0);
    $application=$inmsg->get_object(1);
    $a=$inmsg->get_object(2);

	$name=addslashes($a->name);
	$content=addslashes($a->content);

    $qry = "INSERT INTO calendars (userid,start,end,stext,ltext,origin) VALUES (" ;

	$values = "'$userid','$a->start','$a->end','$name','$content','$application'";
	$values .= ")";
	$qry .= $values;

    $res = $mysql->qry_boolean($qry);
    // just reread !

    $gm->add_object($name);
    return $gm;
}

function put_budget($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $budget=$inmsg->get_object(0);

    $values = "('" . $budget->id . "','" .
          $budget->start . "','" .
          $budget->end . "','" .
          $budget->unit . "','" .
          $budget->amount . "','" .
          $budget->activitytypeid . "','" .
          $budget->priority . "')" ;

    $qry = "REPLACE into budgets" .
          " (id,start,end,unit,amount,activitytypeid,priority) VALUES ".  $values;

    $res = $mysql->qry_boolean($qry);
    // just reread !

    return get_settings($inmsg);
}

function del_budget($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $id=$inmsg->get_object(0);

    $qry = "DELETE from budgets where id=$id";

    $res = $mysql->qry_boolean($qry);
    // just reread !

    return get_settings($inmsg);
}

class Activity
{
    function Activity($start,$end,$name) 
    {
        $this->dur = Round(($end-$start)/60);
        $this->start=$start;
        $this->end=$end;
        $this->name=$name;
    }
}

class Day
{
    function Day($ds) {
        $this->daystring=$ds;
        $this->acts= array();
        $this->atyps= array();
        $this->nacts=0;
    }

    function add_activity($start,$end,$name) {
        $this->atyps[$type] += $end-$start;
        $this->acts[$this->nacts] = new Activity($start,$end,$name);
        $this->nacts++;
    }
}

function hour_string($t)
{
    $tm = localtime($t,true);
    $hour = $tm['tm_hour'];
    $min = $tm['tm_min'];

    if ($hour<10) $hour='0'.$hour;
    if ($min<10) $min='0'.$min;
    return $hour . ":" . $min;
}

function generate_html($mysql,$start,$end,$weeknr,$user,$mime_boundary)
{
	$msg  = "\n--$mime_boundary\n";
	$msg .= "Content-Type: text/html; charset=UTF-8\n";
	$msg .= "Content-Transfer-Encoding: 8bit\n\n";
	$msg .= "<html>\n";

	$msg .= "</html>\n";
	$msg .= "--$mime_boundary--\n\n";

	return $msg;
}

function generate_body($mysql,$start,$end,$weeknr,$user,$mime_boundary)
{
    $daywid=20;

	$html  = "\n--$mime_boundary\n";
	$html .= "Content-Type: text/html; charset=UTF-8\n";
	$html .= "Content-Transfer-Encoding: 8bit\n\n";
	$html .= "<html>\n";

    //$qry = "select * from users where id='" . $user . "'";
    $qry = "SELECT u.name as name,u.firstname,u.lastname,u.administrator,s.name as manager,s.email FROM users as u left join users as s on u.manager=s.id where u.id =$user";
    $res = $mysql->qry_assoc($qry);

    $loginname=$res[0]['name'];
    $email=$res[0]['email'];
    $manager=$res[0]['manager'];

    $msg  = "--$mime_boundary\n";
    $msg .= "Content-Type: text/plain; charset=UTF-8\n";
    //$msg .= "Content-Transfer-Encoding: 8bit\n\n";

    $html .= 'Bevestigingsverzoek van ' . $res[0]["firstname"] . ' ' . $res[0]["lastname"] . " voor week $weeknr<br><br>";
    $msg .= 'Bevestigingsverzoek van ' . $res[0]["firstname"] . ' ' . $res[0]["lastname"] . " voor week $weeknr\n\n";

    $qry = "SELECT start,end,name,at.id from activities left join activitytypes as at on activitytypeid=at.id WHERE " . 
            "userid=" . $user . 
            " AND start>=$start AND start<$end ORDER by start";
    $res = $mysql->qry_assoc($qry);

    $days = array();
    $acts = array();

    foreach ($res as $a) {
        $astart = $a['start']/1000;
        $aend = $a['end']/1000;
        $type = $a['activitytypeid'];
        $name = $a['name'];
        $tm = localtime($astart,true);
        $weekday = $tm['tm_wday'];
        $year = $tm['tm_year']+1900;
        $month = $tm['tm_mon']+1;
        if ($month < 10) $month = '0'.$month;
        $mday = $tm['tm_mday'];
        if ($mday < 10) $mday = '0'.$mday;
        $day = $tm['tm_year']+1900 ."-". $month . "-" . $mday;
        if (!$days[$weekday]) 
            $days[$weekday] = new Day($day);
        else if ($day->daystring != $day) {
            // TODO handle bigger than week  (error)
        }
        $days[$weekday]->add_activity($astart,$aend,$name);
        $acts[$type] += $aend-$astart;
    }
    $html .= "<br>\n";
    $msg .= "\n";

    $tdur=0;
    $ttot= array();
    // stats per day :
    foreach ($days as $day) {
        $msg .= "\n\n" . $day->daystring . ":\n\n";
        $html .= "<br><br>" . $day->daystring . ":<br><br>\n";
        $dur=0;
        $tday= array();
        foreach ($day->acts as $act) { 
            $astart = hour_string($act->start);
            $aend   = hour_string($act->end);
            $dur += $act->dur;
            $tdur += $act->dur;
            $tday[$act->name] += $act->dur;
            $ttot[$act->name] += $act->dur;
        }
        foreach (array_keys($tday) as $key) {
            $d = $tday[$key];
			$h = Floor($d/60);
			$m = $d-($h*60);
			if ($m<10) $m = '0'.$m;
			$msg .= $h.":".$m;
            $msg .= " " . $key . ": \n";
			$html .= $h.":".$m;
            $html .= " " . $key . ": <br>\n";
            //$msg .= $d . " minuten, " . Round(($d*100)/60)/100 . " uur\n";
        }
	$h = 
        $msg .= "-----------------\ntotaal : " ;
        $html .= "-----------------<br>\ntotaal : " ;
	$h = Floor($dur/60);
	$m = $dur-($h*60);
	if ($m<10) $m = '0'.$m;
	$msg .= $h.":".$m;
	$html .= $h.":".$m;
    }
    $msg .= "\n\n\nweek $weeknr\n";
    $html .= "<br><br><br>week $weeknr<br>\n";
    foreach (array_keys($ttot) as $key) {
        $d = $ttot[$key];
		$h = Floor($d/60);
		$m = $d-($h*60);
		if ($m<10) $m = '0'.$m;
		$msg .= $h.":".$m;
        $msg .= " " . $key . ": \n";
		$html .= $h.":".$m;
        $html .= " " . $key . ": <br>\n";
        //$msg .= $d . " minuten, " . Round(($d*100)/60)/100 . " uur\n";
    }
    $msg .= "-----------------\ntotaal : ";
    $html .= "-----------------<br>totaal : \n";
	$h = Floor($tdur/60);
	$m = $tdur-($h*60);
	if ($m<10) $m = '0'.$m;
	$msg .= $h.":".$m . " uur\n\n\n";
	$html .= $h.":".$m . " uur<br><br><br>\n";
	// refererer sometimes gives the script and path, sometimes
	// only the path, so forgive me this little hack :
	// http://demo.adaptime.nl/adaptime/forcetodirectory and
	// http://demo.adaptime.nl/adaptime/adapreg.htmlforcetodirectory  both
	// yield : http://demo.adaptime.nl/adaptime
    $referer= $_SERVER["HTTP_REFERER"] . "forcetodirectory";
    $dir = dirname($referer);

    $html .= "Gebruik onderstaande links om goed of af te keuren, u kunt de links desnoods meerdere malen gebruiken, bedenk echter dat er telkens een email wordt verzonden naar de werknemer<br><br>";
    $html .= "\n<a href=$dir/confirm.html?action=approve&name=" . $loginname . "&manager=$manager&week=$weeknr&start=".$start."&end=".$end.">Goedkeuren</a><br><br>";
    $msg .= "direct bevestigen/bevriezen: (meldt nog even dat de week is goedgekeurd, logt NIET uit)\n  $dir/confirm.html?action=approve&name=" . $loginname . "&manager=$manager&week=$weeknr&start=".$start."&end=".$end."\n\n";
    $html .= "\n<a href=$dir/confirm.html?action=disapprove&name=" . $loginname . "&manager=$manager&week=$weeknr&start=".$start."&end=".$end.">Afkeuren</a><br><br>";
    $msg .= "direct afkeuren/vrijgeven: (logt NIET uit)\n $dir/confirm.html?action=disapprove&name=" . $loginname . "&manager=$manager&week=$weeknr&start=".$start."&end=".$end."\n\n";

    $html .= "\n<a href=$dir/confirm.html?action=explain&name=" . $loginname . "&manager=$manager&week=$weeknr&start=".$start."&end=".$end.">Afkeuren met toelichting</a><br><br>";
    $msg .= "Afkeuren met toelichting: (gaat naar de web page voor meer opties)\n $dir/confirm.html?action=explain&name=" . $loginname . "&manager=$manager&week=$weeknr&start=".$start."&end=".$end."\n\n";

    $html .= "\n<a href=$dir/tutadmin.html>Help / wat is dit ?</a><br><br>";
    $msg .= "\n\n, u kunt de links meerdere malen gebruiken maar elke keer wordt er mail verzonden, de laatste actie geldt.";
	$html .= "</html>\n";
	$html .= "--$mime_boundary--\n\n";

    return $msg . $html;
}

// this should be altered to fire a write on ivo !!
function mark_as($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) 
    return adaptime_error($inmsg,$mysql->err);

    $uid=$inmsg->get_object(0);
    $start=$inmsg->get_object(1);
    $end=$inmsg->get_object(2);
    $status=$inmsg->get_object(3);

    $mime_boundary = "----Adaptime----".md5(time());

    $qry = "UPDATE activities " .
      " set status='" . $status . 
          "' WHERE `userid`= '" . $uid .
          "' AND `start`>= " . $start .
          " AND `start`< " . $end;

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    // Additional headers
    $headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
    $headers .= 'From: Adaptime Mailer <noreply@demo.regas.nl>' . "\r\n";
    
    $res = $mysql->qry_boolean($qry);
    if ($status=="waiting") {
        $weeknr=$inmsg->get_object(4);
        $email=$inmsg->get_object(5);
        $body = generate_body($mysql,$start,$end,$weeknr,$uid,$mime_boundary);
        // TODO : mail address from database 
        mail($email, "Verzoek Bevestiging", $body, $headers);
    } else {
        $email=$inmsg->get_object(4);
        $header=$inmsg->get_object(5);
        $body=$inmsg->get_object(6);
    	$referer= $_SERVER["HTTP_REFERER"];
    	$dir = dirname($referer);
	if ($status == 'disapproved') {
    		$body .= "Voor verbeteringen : " . $dir . "/index.html?weekdate=$start";
	}
        mail($email, $header, $body, $headers);
    }
    return get_activitytimes($inmsg,$mysql);
}

function lock_type($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $tid=$inmsg->get_object(0);

    $qry = "UPDATE activitytypes set editable='0' WHERE id='" . $tid . "'";
	echo $qry;
    $res = $mysql->qry_boolean($qry);
    return $gm;
}

function free_type($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $tid=$inmsg->get_object(0);

    $qry = "UPDATE activitytypes set editable='1' WHERE id='" . $tid . "'";
	echo $qry;
    $res = $mysql->qry_boolean($qry);
    return $gm;
}

function enable_at($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $uid=$inmsg->get_object(0);
    $aid=$inmsg->get_object(1);

    if ($aid < 0) { //enable all by clearing lines
       	$qry = "DELETE FROM useractivitytypes WHERE userid='" . $uid . "'";
    } else { // disable 1 by adding line
       	$qry = "DELETE FROM useractivitytypes WHERE userid='" . $uid . "' AND activitytypeid='" . $aid . "'" ;
    }

    $res = $mysql->qry_boolean($qry);
    return $gm;
}

function disable_at($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);
    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $uid=$inmsg->get_object(0);
    $aid=$inmsg->get_object(1);

   	if ($aid < 0) { //enable all by clearing lines
   		return adaptime_error($inmsg,"disable all is not allowed");
   	} else { // disable 1 by adding line
       	$qry = "REPLACE INTO useractivitytypes (userid,activitytypeid) VALUES ('$uid','$aid')";
   	}

    $res = $mysql->qry_boolean($qry);
    return $gm;
}

function logout()
{
    // If you are using session_name("something"), don't forget it now!
    session_start();

    // Unset all of the session variables.
    $_SESSION = array();

    // If it's desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if (isset($_COOKIE[session_name()])) {
        setcookie(session_name(), '', time()-42000, '/');
    }

    // Finally, destroy the session.
    session_destroy();
}

function login_user($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $logobj = $inmsg->get_object(0);

	// query before usersettings was split off
    //$qry = "SELECT u.id,u.name,u.pass,u.firstname,u.lastname,u.administrator,u.outlook,u.gcalendar,s.name as manager,s.email FROM users as u left join users as s on u.manager=s.id where u.name = '" . $logobj->lname . "'";
    $qry = "SELECT u.id,u.name,u.pass,u.firstname,u.lastname,u.administrator,s.name as manager,s.email FROM users as u left join users as s on u.manager=s.id where u.name = '" . $logobj->lname . "'";
    $res = $mysql->qry_assoc($qry);

    if (!$res) {
        //logout();
        $gm = adaptime_error($inmsg,"login");
    } else {
        $pass = strtolower($res[0]['pass']);
        $admin = $res[0]['administrator'];
		$id = $res[0]['id'];
        if ($logobj->uname != $logobj->lname && $admin == 0) {
            $gm = adaptime_error($inmsg,"admin");
        } else 
        if ($pass == $logobj->pass) {
            $_SESSION["user"]=$logobj->lname;

            $qry = "SELECT u.id,u.name,u.pass,u.firstname,u.lastname,u.administrator,s.name as manager,s.email FROM users as u left join users as s on u.manager=s.id where u.name = '" . $logobj->uname . "'";
            $res = $mysql->qry_assoc($qry);

        $res[0]['firstname'] = htmlentities($res[0]['firstname']);
        $res[0]['lastname'] = htmlentities($res[0]['lastname']);
        $res[0]['email'] = htmlentities($res[0]['email']);
		$id = $res[0]['id'];
            if (!$res) {
                $gm = adaptime_error($inmsg,"login");
            }

            $gm->add_object($res[0]);
        } else {
            //logout();
            $gm = adaptime_error($inmsg,"pass");
        }
		// get usersettings
		$qry = "SELECT * FROM usersettings WHERE userid='". $id ."'";
        $res = $mysql->qry_assoc($qry);
        $gm->add_object($res);
    }
    return $gm;
}

function get_templates($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $pers_id = $inmsg->get_object(0);
    $weekday = $inmsg->get_object(1);

    // ALL activities for this person to be able to calc budgets
    $qry = "SELECT * from templates WHERE " . 
            "userid=" . $pers_id;

	if ($weekday>=0) 
            $qry .= " AND weekday=" . $weekday;
    
    // for testing the busy cursor , sleep a second
    //sleep(1);
    $res = $mysql->qry_assoc($qry);

    $gm->add_object($res);
    return $gm;
}

function get_activitytimes($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $pers_id = $inmsg->get_object(0);
    $start = $inmsg->get_object(1);
    $end = $inmsg->get_object(2);

    $qry = "SELECT * from activities WHERE " . 
            "userid=" . $pers_id . 
            " AND start>=$start AND start<$end " . 
			" ORDER by moveable DESC,activitytypeid,id";
    
    // for testing the busy cursor , sleep a second
    //sleep(1);
    $res = $mysql->qry_assoc($qry);
    $gm->add_object($res);

    // AND ALL activities from extranal applications
    $qry = "SELECT * from calendars WHERE " . 
            "userid=" . $pers_id . 
            " AND (start>=$start OR start<$end) ORDER by id";
    
    // for testing the busy cursor , sleep a second
    //sleep(1);
    $res = $mysql->qry_assoc($qry);
    $gm->add_object($res);

    return $gm;
}

function get_worktimes($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $pers_id = $inmsg->get_object(0);
    $generic_id = $inmsg->get_object(1);

    $qry = "SELECT * FROM worktimes " .
        "WHERE userid = " . $pers_id;

    $res = $mysql->qry_assoc($qry);
    $gm->add_object($res);

    $qry = "SELECT * FROM worktimes " .
        "WHERE userid = " . $generic_id;

    $res = $mysql->qry_assoc($qry);
    $gm->add_object($res);

    return $gm;
}

// the 'old' and probably future 'new' way,
	// table worktimes was previously 2 columns: personid,patternid
function get_workpatterns($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $pers_id = $inmsg->get_object(0);
    //$weekday = $inmsg->get_object(1);
    $generic_id = $inmsg->get_object(1);

    $qry = "SELECT * FROM worktimes " .
        "WHERE userid = " . $pers_id;

    $res = $mysql->qry_assoc($qry);

    $tp = new TimePattern(null,null);
    if (count($res) > 0) 
        $tp->read($mysql,$res[0]['patternid']);
    $gm->add_object($tp);

    // generic template as a backup (uid is -1)
    $qry = "SELECT * FROM worktimes " .
        "WHERE userid = " . $generic_id;

    $res = $mysql->qry_assoc($qry);
    $tp2 = new TimePattern(null,null);
    $tp2->read($mysql,$res[0]['patternid']);
       $gm->add_object($tp2);
    //$gen = array();
    //for ($c=0; $c < count($res) ; $c++) {
    //    $tp2 = new TimePattern(null,null);
        //$tp2->read($mysql,$res[$c]['patternid']);
        //$gen[$c]=$tp2;
    //}
       //$gm->add_object($gen);
    return $gm;
}

function get_users($inmsg,$mysql,$name)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $position = $inmsg->get_object(0);

    $qry = "SELECT * from users";
    if ($name) {
        $qry .= " WHERE name='".$name."'";
    }
    $res = $mysql->qry_assoc($qry);

    if (!$res) {
        //$gm = adaptime_error($inmsg,$mysql->error());
        $gm = adaptime_error($inmsg,$qry);
    } else {
        $gm->add_object($res);
    }
    return $gm;
}

function get_user($inmsg,$mysql)
{
    $pers_id = $inmsg->get_object(0);
    return get_users($inmsg,$mysql,$pers_id);
}

function delete_pos($pos,$mysql)
{
    $qry = "DELETE from activities WHERE id=$pos->id";
    $res = $mysql->qry_boolean($qry);
    return $res;
}

function alter_apptype($pos,$mysql)
{
    $qry = "UPDATE activities" .
          " SET activitytypeid='" .  $pos->activitytypeid . 
          "' WHERE id= " . $pos->id;

    $res = $mysql->qry_boolean($qry);

    return $res;
}

function alter_pos($pos,$mysql)
{
    $qry = "UPDATE activities" .
          " SET start=" .  $pos->start. 
          ",end=" .  $pos->end . 
          ",activitytypeid='" .  $pos->activitytypeid . 
          "' WHERE id= " . $pos->id;

    $res = $mysql->qry_boolean($qry);

    return $res;
}

function delete_tmpl($pos,$mysql)
{
    $qry = "DELETE from templates WHERE id=$pos->id";
    $res = $mysql->qry_boolean($qry);
    return $res;
}

function alter_tmplapptype($pos,$mysql)
{
    $qry = "UPDATE templates" .
          " SET activitytypeid='" .  $pos->activitytypeid . 
          "' WHERE id= " . $pos->id;

    $res = $mysql->qry_boolean($qry);

    return $res;
}

function alter_tmpl($pos,$mysql)
{
    $qry = "UPDATE templates" .
          " SET start=" .  $pos->start. 
          ",end=" .  $pos->end . 
          ",activitytypeid='" .  $pos->activitytypeid . 
          "',weekday='" .  $pos->wd . 
          "' WHERE id= " . $pos->id;

    $res = $mysql->qry_boolean($qry);

    return $res;
}

function check_tmpl_overlap($position,$mysql)
{
    $gm=null;
    $qry = "SELECT * FROM templates " .
            " WHERE id != " . $position->id . 
            " AND user_id = " . $position->userid . 
            " AND weekday = " . $position->wd . 
            " AND startTime >= " . $position->start . 
            " AND startTime < " . $position->end ;

    $res = $mysql->qry_array($qry);
    if (count($res) > 0)  
        $gm = adaptime_error($inmsg,"overlapping times");

    return $gm;
}

function insert_pos($pos,$mysql)
{
    $err = check_tmpl_overlap($pos,$mysql);
    if ($err) return $err;

    // TODO select any autonomous workflowitemdefinitionid, maybe amw1_38
    // is not present !!

    if ($pos->id == -1) {
        $id="";     // force auto_increment
    } else {
        $id=$pos->id;
    }

	$stxt=addslashes($pos->stxt);
	$ltxt=addslashes($pos->ltxt);

    $qry = "INSERT INTO activities " .
          "(id,userid,activitytypeid,start,end,moveable,stext,ltext,source) values ('" .
          $id . "','" .
          $pos->userid . "','" .
          $pos->activitytypeid . "','" .
          $pos->start . "','" .
          $pos->end . "','1','" .
		  $stxt . "','" .
		  $ltxt . "','" .
		  $pos->source . "')";

    $res = $mysql->qry_boolean($qry);
    $id = $mysql->insert_id();
    return $qry;
}

function insert_tmpl($pos,$mysql)
{
    $err = check_tmpl_overlap($pos,$mysql);
    if ($err) return $err;

    // TODO select any autonomous workflowitemdefinitionid, maybe amw1_38
    // is not present !!

    if ($pos->id == -1) {
        $id="";     // force auto_increment
    } else {
        $id=$pos->id;
    }

    $qry = "INSERT INTO templates " .
          "(id,userid,activitytypeid,start,end,weekday) values ('" .
          $id . "','" .
          $pos->userid . "','" .
          $pos->activitytypeid . "','" .
          $pos->start . "','" .
          $pos->end . "','" .
          $pos->wd . "')";

    $res = $mysql->qry_boolean($qry);
    $id = $mysql->insert_id();
    return $qry;
}

function alter_template($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $frompos = $inmsg->get_object(1);
    $topos = $inmsg->get_object(2);

    $res = delete_tmpl($frompos,$mysql);
    if ($res)
        $res = insert_tmpl($topos,$mysql);

    if (!$res) {
        $gm = adaptime_error($inmsg,$mysql->error());
    } else {
        $gm->add_object($res);
    }

    return $gm;
}

function insert_template($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $topos = $inmsg->get_object(1);

    $res = insert_tmpl($topos,$mysql);
    if (!$res) {
        $gm = adaptime_error($inmsg,$res);
    } else {
        $gm->add_object($res);
    }

    return $gm;
}

function delete_template($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $frompos = $inmsg->get_object(1);

    $res = delete_tmpl($frompos,$mysql);

    if (!$res) {
        $gm = adaptime_error($inmsg,$res);
    } else {
        $gm->add_object($res);
    }

    return $gm;
}

function update_template($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    // start at 1, object 0 was the type !!
    for ($n=1; $n<$inmsg->get_object_count(); $n++) {
        $pos = $inmsg->get_object($n);
        $res = alter_tmpl($pos,$mysql);
    }

    return $gm;
}

function update_templatetype($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    // start at 1, object 0 was the type !!
    for ($n=1; $n<$inmsg->get_object_count(); $n++) {
        $pos = $inmsg->get_object($n);
        $res = alter_tmplapptype($pos,$mysql);
    }

    return $gm;
}

function alter_app($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $frompos = $inmsg->get_object(1);
    $topos = $inmsg->get_object(2);

    $res = delete_pos($frompos,$mysql);
    if ($res)
        $res = insert_pos($topos,$mysql);

    if (!$res) {
        $gm = adaptime_error($inmsg,$mysql->error());
    } else {
        $gm->add_object($res);
    }

    return $gm;
}

function insert_app($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $topos = $inmsg->get_object(1);

    $res = insert_pos($topos,$mysql);
    if (!$res) {
        $gm = adaptime_error($inmsg,$res);
    } else {
        $gm->add_object($res);
    }

    return $gm;
}

function delete_app($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $frompos = $inmsg->get_object(1);

    $res = delete_pos($frompos,$mysql);

    if (!$res) {
        $gm = adaptime_error($inmsg,$res);
    } else {
        $gm->add_object($res);
    }

    return $gm;
}

function update_app($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    // start at 1, object 0 was the type !!
    for ($n=1; $n<$inmsg->get_object_count(); $n++) {
        $pos = $inmsg->get_object($n);
        $res = alter_pos($pos,$mysql);
    }

    return $gm;
}

function update_apptype($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    // start at 1, object 0 was the type !!
    for ($n=1; $n<$inmsg->get_object_count(); $n++) {
        $pos = $inmsg->get_object($n);
        $res = alter_apptype($pos,$mysql);
    }

    return $gm;
}

function change_template($inmsg,$mysql)
{
    // from Event.js
    $event_idle   =0; 
    $event_delete =1;
    $event_insert =2;
    $event_move   =3;
    $event_resize =4;
    $event_apptype=5;

    $type = $inmsg->get_object(0);

    switch ($type) {
        case $event_idle:
        break;
        case $event_delete:
            $gm = delete_template($inmsg,$mysql);
        break;
        case $event_insert:
            $gm = insert_template($inmsg,$mysql);
        break;
        case $event_move:
            $gm = update_template($inmsg,$mysql);
        break;
        case $event_resize:
            $gm = update_template($inmsg,$mysql);
        break;
        case $event_apptype:
            $gm = update_templatetype($inmsg,$mysql);
        break;
        default:
            $gm = adaptime_error($inmsg,$event_move);
        break;
    }

    return $gm;
}

function change_app($inmsg,$mysql)
{
    // from Event.js
    $event_idle   =0; 
    $event_delete =1;
    $event_insert =2;
    $event_move   =3;
    $event_resize =4;
    $event_apptype=5;

    $type = $inmsg->get_object(0);

    switch ($type) {
        case $event_idle:
        break;
        case $event_delete:
            $gm = delete_app($inmsg,$mysql);
        break;
        case $event_insert:
            $gm = insert_app($inmsg,$mysql);
        break;
        case $event_move:
            $gm = update_app($inmsg,$mysql);
        break;
        case $event_resize:
            $gm = update_app($inmsg,$mysql);
        break;
        case $event_apptype:
            $gm = update_apptype($inmsg,$mysql);
        break;
        default:
            $gm = adaptime_error($inmsg,$event_move);
        break;
    }

    return $gm;
}

function get_color($arr,$i)
{
    if ($i > count ($arr)) 
        return "black"; // TODO : random colors
    else 
        return $arr[$i]['colorname'];
}

function array_data($inmsg,$mysql)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    $personid = $inmsg->get_object(0);

    if (!$mysql) $mysql = open_proxy_db($gm);
    if ($mysql->err) return adaptime_error($inmsg,$mysql->err);

    $qry = "SELECT * FROM colors order by colorid";
    $clrs = $mysql->qry_assoc($qry);

    $qry = "SELECT * FROM activitytypes";
    $res = $mysql->qry_assoc($qry);

    for ($l = 0; $l < count($res); $l++) {
        if ($res[$l]['color'] == "" || $res[$l]['color'] == "auto")
        {
            $res[$l]['color'] = get_color($clrs,$l);
        }
        $res[$l]['name'] = rawurlencode($res[$l]['name']);
    }

    if (!$res) {
        $gm = adaptime_error($inmsg,"array_data");
    } else {
        $gm->add_object($res);
    }

    // and the active ones
    $qry = "SELECT * FROM useractivitytypes WHERE userid=". $personid;
    $res = $mysql->qry_assoc($qry);

    $gm->add_object($res);

    return $gm;
}

function get_texts($inmsg)
{
    $gm = new JsonResult("ok", $inmsg->cmd,null);

    $which = $inmsg->get_object(0);
	$res= get_strings($which);

    if (!$res) {
        $gm = adaptime_error($inmsg,"get_texts");
    } else {
        $gm->add_object($res);
    }
    return $gm;
}

class returnobj
{
    function returnobj($x,$y) 
    {
        $this->x =$x;
        $this->y =$y;
    }
}

if ($json) {
    $jm = new JsonMessage($json);

    if ($jm->cmd != "get_settings" && $jm->cmd != "login" && ! $_SESSION["user"]) {
        $e = adaptime_error($inmsg,"not logged in");
        $e->put();
    exit(0);
    }

    // dispatch on cmd
    switch($jm->cmd) {
        case "login":
            $e=login_user($jm,null);
        break;
        case "logout":
            logout();
            $e = new JsonResult("ok", $inmsg->cmd,null);
        break;
        case "array_data":
            $e=array_data($jm,null);
        break;
        case "update_template":
            $e=update_template($jm,null);
        break;
        case "update_templatetype":
            $e=update_templatetype($jm,null);
        break;
        case "change_template":
            $e=change_template($jm,null);
        break;
        case "update_app":
            $e=update_app($jm,null);
        break;
        case "update_apptype":
            $e=update_apptype($jm,null);
        break;
        case "change_app":
            $e=change_app($jm,null);
        break;
        case "get_worktimes":
            $e=get_worktimes($jm,null);
        break;
        case "get_activitytimes":
            $e=get_activitytimes($jm,null);
        break;
        case "get_templates":
            $e=get_templates($jm,null);
        break;
        case "get_users":
            $e=get_users($jm,null);
        break;
        case "get_user":
            $e=get_user($jm,null);
        break;
        case "get_settings":
            $e=get_settings($jm);
        break;
        case "get_userbudget":
            $e=get_userbudget($jm);
        break;
        case "set_userbudget":
            $e=set_userbudget($jm);
        break;
        case "set_usersync":
            $e=set_usersync($jm);
        break;
        case "get_timepattern":
            $e=get_timepattern($jm);
        break;
        case "get_budgets":
            $e=get_budgets($jm);
        break;
        case "get_allbudgets":
            $e=get_allbudgets($jm);
        break;
        case "put_budget":
            $e=put_budget($jm);
        break;
        case "put_calendar":
            $e=put_calendar($jm);
        break;
        case "clr_calendar":
            $e=clr_calendar($jm);
        break;
        case "del_budget":
            $e=del_budget($jm);
        break;
        case "put_setting":
            $e=put_setting($jm);
        break;
        case "mark_as":
            $e=mark_as($jm);
        break;
        case "lock_type":
            $e=lock_type($jm);
        break;
        case "free_type":
            $e=free_type($jm);
        break;
        case "enable_at":
            $e=enable_at($jm);
        break;
        case "disable_at":
            $e=disable_at($jm);
        break;
        case "get_texts":
            $e=get_texts($jm);
        break;
        default:
			print_r($json);
            echo "unknown json command:".$jm->cmd."|";
        break;
    }
    $e->put();
} else {
    // AjaxContext examples
    switch ($cmd) {
        default:
            //echo "ask only uses Json messages";
       print_r($_POST);
        break;
    }
}

?>
