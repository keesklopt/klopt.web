lang_strings = {
    "lang-select" : { 
        "nl": "Kies een andere taal",
        "en": "Select other language",
    },
    "lang-info" : { 
        "nl": "Logistieke oplossingen <br>klopt.org",
        "en": "Logistical solutions<br> klopt.org",
    },
    "lang-network" : { 
        "nl": "Dagelijks nieuwe wegnetwerk data",
        "en": "Daily road network updates",
    },
    "lang-demo" : { 
        "nl": "Demonstraties",
        "en": "Demonstrations",
    },
    "lang-demonstration" : { 
        "nl": "Klik hier voor live applicaties",
        "en": "Click for live applications",
    },
    "lang-cloud" : { 
        "nl": "Cloud gebaseerd",
        "en": "Cloud based services",
    },
    "lang-vrp" : { 
        "nl": "Vrp",
        "en": "Vrp",
    },
    "lang-veerpee" : { 
        "nl": "Ritten planning",
        "en": "Vehicle route planning",
    },
    "lang-map" : { 
        "nl": "Track<br>Trace",
        "en": "Track<br>Trace",
    },
    "lang-mobile" : { 
        "nl": "Mobiel",
        "en": "Mobile",
    },
    "lang-routing" : { 
        "nl": "Routering",
        "en": "Routing",
    } 
}

text_content = { 
    "text-cloud" : { 
        "nl": "cloud_nl.html",
        "en": "cloud_en.html",
    }, 
    "text-routing" : { 
        "nl": "routing_nl.html",
        "en": "routing_en.html",
    },
    "text-demo" : { 
        "nl": "demo_nl.html",
        "en": "demo_en.html",
    },
    "text-vrp" : { 
        "nl": "vrp_nl.html",
        "en": "vrp_en.html",
    },
    "text-mobile" : { 
        "nl": "mobile_nl.html",
        "en": "mobile_en.html",
    },
    "text-map" : { 
        "nl": "map_nl.html",
        "en": "map_en.html",
    },
    "text-lang" : { 
        "nl": "lang_nl.html",
        "en": "lang_en.html",
    },
    "text-network" : { 
        "nl": "network_nl.html",
        "en": "network_en.html",
    },
    "text-klopt" : { 
        "nl": "klopt_nl.html",
        "en": "klopt_en.html",
    },
}

function Language()
{
    var self=this
    this.language="nl"; // default
    // run these after adding items to check for doubles :
    //val = test("id");

    this.reset=function(pfx) {
        list = $("*[id^='lang']").each(
            function (i, el) {
                //console.log(self.language);
                //console.log(el);
                el.innerHTML = lang_strings[el.id][self.language];
         });

         list = $("*[id^='text']").each(
            function (i, el) {
                el.src = text_content[el.id][self.language];
         });

        // switch flags front and back
        if (self.language == "nl") { 
            nldisplay="inline";
            endisplay="none";
            $("img[id='flag-one']").get(0).src = "images/nl.jpg";
            $("img[id='flag-two']").get(0).src = "images/en.jpg";
        } else { 
            nldisplay="none";
            endisplay="inline";
            $("img[id='flag-one']").get(0).src = "images/en.jpg";
            $("img[id='flag-two']").get(0).src = "images/nl.jpg";
        } 

        /*
        $(".english").each(
            function (i, el) {
                el.style.display=endisplay;
        });
        $(".dutch").each(
            function (i, el) {
                el.style.display=nldisplay;
        });
        */
    } 

    this.change=function() {
        if (self.language == "en") { 
            self.language = "nl";
            this.reset();
        } else 
        if (self.language == "nl") { 
            self.language = "en";
            this.reset();
        } 
    } 

    this.current=function() 
    {
        return this.language
    }

    function test(mem) {
        lang_strings.sort(function(a,b) { 
                if (a[mem] < b[mem]) return -1;
                if (a[mem] > b[mem]) return  1;
                return 0;
            } );
        text_content.sort(function(a,b) { 
                if (a[mem] < b[mem]) return -1;
                if (a[mem] > b[mem]) return  1;
                return 0;
            } );
        var last = false;
        var current=false;
        for (s in lang_strings) {
            current=lang_strings[s];
            if (last && last[mem] == current[mem]) { 
                alert("Member " + mem + " has double entry " + last[mem]);
            } 
            last = current;
        } 
        for (s in text_content) {
            current=text_content[s];
            if (last && last[mem] == current[mem]) { 
                alert("Member " + mem + " has double entry " + last[mem]);
            } 
            last = current;
        } 
    } 

    this.reset();
} 
