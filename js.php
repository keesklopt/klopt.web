<?php  

# js version, makes json requests to pass to server

require_once "ajax.php";

$cmd=$_GET['cmd'];
$data=$_GET['data'];

$arr = [
 "Restaurant De Jufferen Lunsingh","Hoofdweg","13","9337 PA","Westervelde" ,
 "Il Gusto","Luttekepoortstraat","36","3841 AX","Harderwijk" ,
 "Restaurant de Librije","Broerenkerkplein","13","8011 TW","Zwolle" ,
 "IJsboerderij Bonestro","Bovenweg","34","8071 ST","Nunspeet" ,
 "Landgoed Lauswolt","van Harinxmaweg","10","9244 CJ","Beetsterzwaag" ,
 "Hotel/Restaurant De Zwaan","Kerkstraat","2","8102 EA","Raalte" ,
 "De Koperen Hoogte","Lichtmisweg","51","8035 PL","Zwolle" ,
 "Librije Zusje / spinhuis","Spinhuisplein","1","8011 ZZ","Zwolle" ,
 "Restaurant De Salentein","Putterstraatweg","7","3862 RA","Nijkerk GL" ,
 "Restaurant Over de Tong","Willemskade","7","8011AC","Zwolle" ,
 "Restaurant De Zon","Voorbrug","1","7731 BB","Ommen" ,
 "Restaurant De Gastronoom","Voorstraat","38","8861 BM","Harlingen" ,
 "De Groene Lantaarn","Hoogeveenseweg","17","7921 PC","Zuidwolde DR" ,
 "Restaurant Eindeloos","Korfmakersstraat","17","8911 LA","Leeuwarden" ,
 "Restaurant de Vijf Sinnen","Hegedijk","2","9024 EA","Weidum" ,
 "MARFO B.V.","Postbus","137","8200 AC","Lelystad" ,
 "Hotel Sterrenberg","Houtkampweg","1","6731 AV","Otterloo" ,
 "Auberge aan het Hof","Kerkstraat","9","8356 DN","Blokzijl" ,
 "Gasterij De Oale Marckt","Marktstraat","18","7642 AL","Wierden" ,
 "Meyers Culicafe","Waterlelie","15","3648 NT","Wilnis" ,
 "Smits Specialiteiten","Petuniastraat","16","3911 WJ","Rhenen" ,
 "Rico y Mas","Molterheurner7","","7587LG","De Lutte" ,
 "Wijnhandel Zaagemans","Schrans","84","8932nh","Leeuwarden" ,
 "Restaurant de Herberg","Hoogeveenseweg","27","7931 TD","Fluitenberg" ,
 "Kasteel De Vanenburg","Vanenburgerallee","13","3882 RH","Putten" ,
 "Thermen Bussloo","Bloemenksweg","38","7383RN","Voorst" ,
 "NH Hotel de Ville (Bistro Het Gerecht)","Postbus","19033","3501 DA","Utrecht" ,
 "Restaurant Cunera","Grebbeweg","1","3911AS","Rhenen" ,
 "'t Nonnetje","Vischmarkt","38","3841 BG","Harderwijk" ,
 "Brasserie Bakboord","Veerkade","10","1357PK","Almere" ,
 "Restaurant Frish 'n Dish","Voorstraat","38","8861 BM","Harlingen" ,
 "Eetcafe De Gans","Diephuisstraat","6","9714 GW","Groningen" ,
 "Restaurant De Holtweijde","Spiekweg","7","7635 LP","Lattrop Breklenkamp" ,
 "De Wilgenhoeve","De Warren","2","9203 HT","Drachten" ,
 "Restaurant Mes Amis","Wijk","5","8321 GN","Urk" ,
 "Restaurant Seidel","Kerkplein","1","8325BN","Vollenhove" ,
 "Restaurant de Rheezerbelten","Grote Beltenweg","1","7771 SX","Hardenberg" ,
 "Jan van de Krent","Burgemeester Dijckmeesterweg","27","7201AJ","Zutphen" ,
 "Cafe Restaurant de Hinde","'t Oost 4","","8713 JP","Hindeloopen" ,
 "Frouckje State","Binnendijk","74","9256HP","Ryptsjerk" ,
 "Librije's winkel","Meerminneplein","7","8011 SW","Zwolle" ,
 "Restaurant Kaatje bij de Sluis","Brouwerstraat","20","8356 DV","Blokzijl" ,
 "Adriaan Klop","Simon van Collemstraat","90","1325 NC","Almere" ,
 "Restaurant de Gaffel","Odoornerweg","1","7872 PA","Valthe" ,
 "Restaurant De Lindenhof","Beulakerweg","77","8355 AC","Giethoorn" ,
 "Hof van Saksen (Restaurant Cour du Nord)","Veldweg","22","9449 PW","Nooitgedacht" ,
 "Restaurant De Havixhorst","Postbus","25","7957 ZG","De Wijk" ,
 "Restaurant Het Roode Koper","J.C Sandbergweg 82","","3852 PV","Ermelo" ,
 "Restaurant 't Veerhuys","Damveld","3","1359 HE","Almere" ,
 "Restaurant Diekhuus","Bandijk","2","7396 NB","Terwolde" ,
 "De Kloosterhoeve","Burgemeester de Kockstraat","44","7861 AE","Oosterhesselen" ,
 "Restaurant Basiliek","Vischmarkt","57","3841 BE","Harderwijk" ,
 "Hotel Frederiksoord","Majoor van Swietenlaan","20","8382 CG","Frederiksoord" ,
 "IJssalon Mariola","Kruisstraat","74","1353 AP","Almere" ,
 "Bistro Hemel op Aarde","Deventerpad","32","1324 EG","Almere" ,
 "Golden Tulip De Beyaerd","Harderwijkerweg","497","8077 RJ","Hulshorst" ,
 "Restaurant Lyf's","Hoofdstraat","73","9244 CM","Beetsterzwaag" ,
 "Grand Café De Fanfare","Binnenpad","68","8355 BV","Giethoorn" ,
];

// this takes postcodes like this : 
// "3151aw" "22","2691GK" "24",
// creates json for the server
function calculate($data)
{
	$arr = explode(",", $data);

	$qry ="{\"jsonrpc\":\"2.0\",\"method\":\"geocode\",\"id\":44,\"params\":";
	$qry .= "{\"postcodes\":[";
	$count=0;
	foreach ($arr as $entry) {
		$split = explode(" ",$entry);
		$pc=$split[0];
		$nr=$split[1];
		if ($count++ >0) $qry .= ",";
		$qry .= "{\"p\":\"" . $pc . "\",\"n\":\"" . $nr . "\"}";
		#echo $split[0] . "," . $split[1] . ",55,";
	} 
	$qry .= "]";
	$qry .= "}}\n";

	if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0)))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Couldn't create socket: [$errorcode] $errormsg \n");
	}
 
	#echo "Socket created \n";
	 
	if(!socket_connect($sock , 'localhost' , 8811))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Could not connect: [$errorcode] $errormsg \n");
	}
	 
	#echo "Connection established \n";

	//Send the message to the server
	if( ! socket_send ( $sock , $qry , strlen($qry) , 0))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Could not send data: [$errorcode] $errormsg \n");
	}
 
	$buf = socket_read($sock, 2048, PHP_NORMAL_READ);
	echo $buf; 	// straight passthrough for now 
}

// this takes only one postcode and returns 
function postcode($data)
{
	$arr = explode(",", $data);

	$qry ="{\"jsonrpc\":\"2.0\",\"method\":\"geocode\",\"id\":44,\"params\":";
	$qry .= "{\"postcodes\":[";
	$count=0;
	foreach ($arr as $entry) {
		$split = explode(" ",$entry);
		$pc=$split[0];
		$nr=$split[1];
		if ($count++ >0) $qry .= ",";
		$qry .= "{\"p\":\"" . $pc . "\",\"n\":\"" . $nr . "\"}";
		#echo $split[0] . "," . $split[1] . ",55,";
	} 
	$qry .= "]";
	$qry .= "}}\n";

	if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0)))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Couldn't create socket: [$errorcode] $errormsg \n");
	}
 
	#echo "Socket created \n";
	 
	if(!socket_connect($sock , 'localhost' , 8811))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Could not connect: [$errorcode] $errormsg \n");
	}
	 
	#echo "Connection established \n";

	//Send the message to the server
	if( ! socket_send ( $sock , $qry , strlen($qry) , 0))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Could not send data: [$errorcode] $errormsg \n");
	}
 
	$buf = socket_read($sock, 2048, PHP_NORMAL_READ);
	echo $buf; 	// straight passthrough for now 
}

function clientlist()
{
	$c=0;
	$r=0;
	global $arr;
	
	foreach ($arr as $entry) { 
		if ($c==2) echo $entry . ",";
		if ($c==3) echo $entry . ",";
		$c++; 
		if ($c> 4) { $c=0; $r++; }
	} 
}

switch ($cmd) {
		case "clientlist":
			clientlist();
		break;
		case "calculate":
			calculate($data);
		break;
		case "postcode":
			postcode($data);
		break;
		case "distances":
			distances($data);
		break;
        default:
            //echo "ask only uses Json messages";
       print_r($_POST);
        break;
}

?>
