#!/usr/bin/python
import sys, math, pprint, cgi

from twisted.python import log
from twisted.internet import reactor, defer
from twisted.web.server import Site
from twisted.web.static import File

from threading import Thread
from time import sleep
import Queue

from autobahn.twisted.websocket import listenWS
from autobahn.wamp1.protocol import exportRpc,    \
                      WampServerFactory,    \
                      WampServerProtocol

possible = []
possible.append ({ "name": "Mr White", "color" : "#ffffff", "code" : "w"})
possible.append ({ "name": "Mr Blue", "color" : "#4444dd", "code" : "b"})
possible.append ({ "name": "Mr Blonde", "color" : "#e3d6b4", "code" : "l"})
possible.append ({ "name": "Mr Pink", "color" : "#e5478d", "code" : "p"})
possible.append ({ "name": "Mr Brown", "color" : "#5e4211", "code" : "r"})
possible.append ({ "name": "Mr Orange", "color" : "#edae40", "code" : "o"})

maxclients=len(possible)
clients = {}
stop=False
game=False
# initial dimensions
w=600
h=400

class Player:
    score=0    
    state='active';

    def __init__(self,p):
        self.name=p['name'];
        self.color=p['color'];
        self.code=p['code'];

    def upScore(self):
        print("upScore");
        self.score += 1
        print self.score

    def clearScore(self):
        self.score = 0

    def setState(self,state):
        self.state=state
        
    def init(self,num):
        global w,h
        self.startx=(w/12) * (num*2+1)
        self.starty=h/2 

    def startMove(self):
        try:
            global game
            action = "i:" # what
            action += self.code + ":"    # who
            # how ...
            action += str(self.startx) + ":" + str(self.starty)
            action += ":" + self.color
            game.addAction(action)
        except Exception as e:
            print (e)

class Game:

    def __init__(self,players,protocol):
        self.players=players
        self.protocol=protocol
        self.setStates("active");

    def numActive(self):
        c=0
        for k,p in self.players.items():
            print (p.state)
            if (p.state == "active"): c+=1
        
        return c

    def getWinner(self):
        print ("numactive now ")
        print self.numActive();
        if (self.numActive() != 1): return False;
        for k,p in self.players.items():
            if (p.state == "active"):
                return p
        return False

    def setStates(self,state):
        for k,p in self.players.items():
            p.setState(state) 

    def setState(self,code,state):
        for k,p in self.players.items():
            if (p.code==code):
                try:
                    p.setState(state) 
                except Exception as e:
                    print e

    def init(self):
        x=0
        for k,p in self.players.items():
            p.init(x)
            x += 1
            m = p.startMove()

        self.protocol.flushq()

    def addAction(self,msg):
        print ("addAction " + msg);
        self.protocol.addq(msg)

    def forfeit(self,id):
        self.protocol.forfeit(id)

class CyclesConnection:
    name=False
    code=False
    protocol=False
    queue = Queue.Queue(0)
    #global game

    def __init__(self,ptrcl):
        self.protocol=ptrcl

    def addq(self,msg):
        #print ("add " + msg)
        self.queue.put(msg)
        print self.queue.qsize()
        
    def flushq(self):
        while self.queue.qsize() > 0:
            try:
                msg = self.queue.get_nowait()
            except:
                msg = None
            if msg==None:
                break
            print "flushing " + msg
            self.protocol.dispatch("http://klopt.org/simple/CyclesTickService#",msg)

    def get_new_player(self):   
        for n in possible:
            used = False
            for k,v in clients.items():
                if (n['name'] == v.name): used=True
            if (used==False): return n;
            
        return False

    def remove(self):
        del clients[self.name]

    def score(self):
        msg = "score:";
        for k,v in clients.items():
            msg += v.code
            msg += '-'
            msg += str(v.score)
            msg += ':'
            self.protocol.dispatch("http://klopt.org/simple/CyclesChatService#",msg)

    @exportRpc
    def id(self):
        p = self.get_new_player()
        self.name = p['name']
        self.code = p['code']
        if (self.name==False):
            print "Refusing more than 6 players";
            return False;

        print "Adding " + self.name;
        player = Player(p)
        clients[self.name] = player
        try:
            self.score()
        except Exception as e:
            print e
        return self.code + ":" + self.name

    @exportRpc
    def snd(self,msg):
        try:
            combi = self.name + "> " + msg + "\n"
        except Exception as e:
            print e;
        self.protocol.dispatch("http://klopt.org/simple/CyclesChatService#",combi)

    @exportRpc
    def startgame(self):
        print "started game"
        global stop;
        stop=False;
        try:
            global game
            game = Game(clients,self)
            game.init();
            ticker = Tick(self.protocol.ccon)
            ticker.start()
        except Exception as e:
            print e

    @exportRpc
    def stopgame(self):
        global stop
        #print "stopped game"
        stop = True

    @exportRpc
    def up(self):
        game.addAction("u:"+ self.code)

    @exportRpc
    def down(self):
        game.addAction("d:"+ self.code)

    @exportRpc
    def left(self):
        game.addAction("l:"+ self.code)

    @exportRpc
    def right(self):
        game.addAction("r:"+ self.code)

    @exportRpc
    def forfeit(self):
        global game
        print self.name + " forfeits"
        # now if all players left forfeit at the same
        # time, i'll decide a winner :
        if (game.numActive()>1): 
            msg = "f:" + self.code
            game.setState(self.code,'dead')
            game.addAction(msg)

        # also message winner if 1 left : 
        try:
            winner = game.getWinner()
            if (winner!= False):
                msg = "w:" + winner.code
                game.setState(winner.code,'winner')
                game.addAction(msg)
                winner.upScore()
                self.score()
        except Exception as e:
            print e
            

    @exportRpc
    def dim(self,wid,hei):
        global w,h,stop
        if (stop==False):   # not during a running game
            return
        w=int(wid)
        h=int(hei)
        msg = "dim:" + wid + ":" + hei + "\n"
        self.protocol.dispatch("http://klopt.org/simple/CyclesChatService#",msg)

    @exportRpc
    def who(self):
        print ("who called");
        msg = "currently logged in :\n"
        for k,v in clients.items():
            msg += v.name;
            if (self.name == v.name):
                msg += "(you)"
            msg += '\n'

        return msg;
            

    @exportRpc
    def help(self):
        helpmessage = "help: this message\n" \
                    "start: start new game\n"  \
                    "who: list who is present\n"  \
                    "clr: clear your chat screen\n" \
                    "dim(w,h): set game dimensions\n"  \
                    
        return helpmessage;

class CyclesServerProtocol(WampServerProtocol):
    def onSessionOpen(self):
        self.ccon = CyclesConnection(self)
        self.registerForRpc(self.ccon, "http://klopt.org/simple/CyclesRpcService#")
        self.registerForPubSub("http://klopt.org/simple/CyclesChatService#", True);
        self.registerForPubSub("http://klopt.org/simple/CyclesTickService#", True);

    def onClose(self,a,b,c):
        print "Removing " + self.ccon.name
        self.ccon.remove()
        self.ccon.score()

class Tick(Thread):

    def __init__(self,ccon):
        Thread.__init__(self) # Thread's constructor has to be called
        self.ccon=ccon

    def run(self):
        global stop, game
        while (stop==False):
            action="-"
            game.protocol.flushq() # first flush all pending actions
            self.ccon.protocol.dispatch("http://klopt.org/simple/CyclesTickService#",action)
            sleep(0.02) # 0.2 is smooth and reasonably quick

if __name__ == '__main__':

    if len(sys.argv) > 1 and sys.argv[1] == 'debug':
        log.startLogging(sys.stdout)
        debug = True
    else:
        debug = False

    try:
        factory = WampServerFactory("ws://localhost:7071", debugWamp = debug)
        factory.protocol = CyclesServerProtocol
        factory.setProtocolOptions(allowHixie76 = True)
        listenWS(factory)

        webdir = File(".")
        web = Site(webdir)
        reactor.listenTCP(7070, web)

        reactor.run()

    except Exception as ee:
        print ("Got an exception : ")
