<?php  

// plain csv version, to keep messages minimal
// this file is meant to pass messages to the server, it is only
// needed in between to strip the http headers , POST version

require_once "ajax.php";

$jsonrpc=$_POST['jsonrpc'];

function json($data)
{
	$qry =$data . "\n";
	
	if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0)))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Couldn't create socket: [$errorcode] $errormsg \n");
	}
 
	#echo "Socket created \n";
	 
	if(!socket_connect($sock , 'localhost' , 6660))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Could not connect: [$errorcode] $errormsg \n");
	}

	//Send the message to the server
	if( ! socket_send ( $sock , $qry , strlen($qry) , 0))
	{
    	$errorcode = socket_last_error();
    	$errormsg = socket_strerror($errorcode);
    	 
    	die("Could not send data: [$errorcode] $errormsg \n");
	}
 
	$buf = socket_read($sock, 100000000, PHP_NORMAL_READ);
	echo $buf; 	// straight passthrough for now 
}

json($jsonrpc);
