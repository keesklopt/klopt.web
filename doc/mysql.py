#!/usr/bin/python
import os
from os import path
import glob
import sys
import json
from hierarchy import hierarchy

class Suggestions:

    def install(self):
        print "install"
        print("no MySQLdb modules found");
        print("you at least need :");
        print("sudo apt-get install mysql-server libmysqlclient-dev python-dev");
        print("sudo apt-get install python-pip");
        print("sudo pip install mysql-python");

    def connect(self,err):
        print "you seem to lack access rights to the database"

    def credentials(self):
        print "to avoid checking credentials into svn, or having to identify each time"
        print "create a file called credentials.py, with two variables :"
        print "uname='your name'"
        print "upass='your pass'"
        print "uhost='mysql host'"
        print "udb='database used'"
        print "and grant the privileges in mysql to the table 'kloptwiki_page' using those values"

try:
    import _mysql
    import MySQLdb as mdb
except:
    Suggestions().install();
    sys.exit(1)
    
try:
    from credentials import uname,upass,udb,uhost
except:
    Suggestions().credentials()
    sys.exit(1)

def empty_table(con):
    with con:
        cur = con.cursor()
        cur.execute("TRUNCATE TABLE `kloptwiki_page`")


# explicitly waved goodbye to django, so it's not in the django db either
try:
    con = mdb.connect(uhost, uname, upass, udb)
    
except _mysql.Error, e:
    print "Error %d: %s" % (e.args[0], e.args[1])
    Suggestions().connect(e.args[0])
    sys.exit(1)
except:
    Suggestions().credentials()
    sys.exit(1)
    

def insert_top():
    # first insert the top page, to be able to link the rest
    for filename in glob.iglob(path.join('top','*.html')):
        with open(filename) as f:
            content = f.read()
            sp = path.splitext(path.basename(filename))
            print (sp[0])
            
            with con:
                cur = con.cursor()
                esceep = content.replace("'","\\'");
                #print esceep
                cur.execute("INSERT INTO kloptwiki_page(name,display,content,parent_id,type) VALUES('" + sp[0] + "','','" + esceep + "','top','top')")

def insert_layout():
    # first insert the top page, to be able to link the rest
    for filename in glob.iglob(path.join('layout','*.html')):
        with open(filename) as f:
            content = f.read()
            sp = path.splitext(path.basename(filename))
            print (sp[0])
            
            with con:
                cur = con.cursor()
                esceep = content.replace("'","\\'");
                #print esceep
                cur.execute("INSERT INTO kloptwiki_page(name,display,content,parent_id,type) VALUES('" + sp[0] + "','','" + esceep + "','top','layout')")

def insert_rest():
    # than insert the restin the current directory
    for filename in glob.iglob(path.join('.','*.html')):
        with open(filename) as f:
            content = f.read()
            sp = path.splitext(path.basename(filename))
            print (sp[0])
            
            with con:
                cur = con.cursor()
                esceep = content.replace("'","\\'");
                #print esceep
                cur.execute("INSERT INTO kloptwiki_page(name,content,parent_id,type) VALUES('" + sp[0] + "','" + esceep + "','top','content')")

def drop_table(con):
    qry = """DROP TABLE IF EXISTS `kloptwiki_page` """
    print(con);
    with con:
        cur = con.cursor()
        cur.execute(qry)
    
# all html goes into the same table, and are selected on type : 
# top : 1 only, it's the top index page
# content : all content pages
# layout : all pages used for page make up, like header footer etc
def create_table(con):
    qry = """CREATE TABLE `kloptwiki_page` (
  `name` varchar(100) NOT NULL,
  `display` varchar(100) NOT NULL,
  `content` longtext NOT NULL,
  `parent_id` varchar(100) NOT NULL,
  `type` varchar(100),
  PRIMARY KEY (`name`),
  FULLTEXT (display,content),
  KEY `kloptwiki_page_63f17a16` (`parent_id`),
  CONSTRAINT `parent_id_refs_name_5feeb255` FOREIGN KEY (`parent_id`) REFERENCES `kloptwiki_page` (`name`)
   ) ENGINE=MyISAM DEFAULT CHARSET=latin1; """
    with con:
        cur = con.cursor()
        cur.execute(qry)

# to be able to recreate the hierarchy we need this as well
def create_hierarchy():
    for key,val in hierarchy.iteritems():
        print (key)
        with con:
            cur = con.cursor()
            qry = "UPDATE kloptwiki_page SET parent_id='" + val[1] + "',display='" + val[0] + "' WHERE name='" + key + "'"
            print (qry)
            cur.execute(qry);

def check_hierarchy():
    "check if all entries match, and print the one that don't"
    qry = "SELECT name from kloptwiki_page where type='content'";
    try:
        with con:
            cur = con.cursor()
            cur.execute(qry)
    
        allfound=True;
    except:
        print "Table does not exist";
        print "Run this scripts create_table() method first";
        return False;

    for row in cur.fetchall() :
        found=False;
        for name in hierarchy:
            if (name==row[0]): found=True;
        if not found:
            allfound = False;
            print "Row '" + row[0] + "' not added to hierarchy !";
            print "Fix hierarchy first, then re-run this script";

    return allfound;

drop_table(con)
create_table(con)
empty_table(con)
insert_top()
insert_layout()
insert_rest()
if not check_hierarchy():
    sys.exit(-1);

create_hierarchy()
