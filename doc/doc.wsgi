#!/usr/bin/python
# a2ensite doc
import sys
import os

# set the path to current dir for importing 
sys.path.append(os.path.dirname(__file__))

import MySQLdb
import markdown

error=False

try:
    from pygments import highlight
    from pygments.lexers import get_lexer_by_name, guess_lexer, TextLexer
    from pygments.formatters import HtmlFormatter
    pygments = True
except ImportError:
    pygments = False

from cgi import parse_qs, escape
try:
    from credentials import uname,upass,udb
except:
    error="No credentials, check your configuration";

class Database():
    def __init__(self):
        self.db = MySQLdb.connect(host="localhost", user=uname, passwd=upass, db=udb)

    def cursor(self):
        return self.db.cursor();


class Page():
    def __init__(self,which,search):
        db = Database();
        self.db = Database();
        self.page = which;
        self.search = search;

    def get_head(self):
        cur = self.db.cursor() 
        qry = "SELECT content FROM kloptwiki_page WHERE type='layout' ";
        qry += " AND name='head'";
        cur.execute(qry);
        head = "";
        for row in cur.fetchall() :
            head += row[0];

        return head;

    def get_layout(self,name):
        cur = self.db.cursor() 
        qry = "SELECT content FROM kloptwiki_page WHERE type='layout' ";
        qry += " AND name='" + name + "'";
        cur.execute(qry);
        html = "";
        for row in cur.fetchall() :
            html += row[0];

        return html;

    def get_search(self):
        cur = self.db.cursor();
        qry = "SELECT name,display FROM kloptwiki_page WHERE type='content' AND MATCH(display,content) AGAINST ('" + self.search + "' IN NATURAL LANGUAGE MODE)";
        cur.execute(qry);
        html = "<div id='content' class='innerBox' style='left: 160px; top:0px; height: 100%; position:absolute; overflow-y: auto;'>";
        for row in cur.fetchall() :
            html += "<li>"
            html += "<a href=\"?page=" + row[0] + "\">" + row[1] + "</a>";
        return "<ul>" + html + "</ul>";

    def get_page(self):
        cur = self.db.cursor() 
        command = "SELECT content FROM kloptwiki_page where name='" + self.page + "'";
        cur.execute(command);
        html = "<div id='content' class='innerBox' style='left: 160px; top:0px; height: 100%; position:absolute; overflow-y: auto;'>";
        xtra = "";
        for row in cur.fetchall() :
            xtra += row[0];
        text = xtra.decode('utf-8');
        html += markdown.markdown(text, extensions=["codehilite"]);
        html += "</div>";
        
        return html

    def get_menu(self):
        cur = self.db.cursor();
        qry  = "SELECT name,display FROM kloptwiki_page where type='content'";
        qry += " AND parent_id='" + self.page + "'";
        cur.execute(qry);

        xtra = "<div id='menu' class='menu' style='width:160px; left: 0px; top:0px; height: 100%; position:absolute;'>";
        result = cur.fetchall();

        if (len(result)>0): # prevent empty div border
            xtra += "<div class='menudiv'>";
            xtra += "<ul>";
            for row in result :
                xtra += "<li><a href=\"?page=" + row[0] + "\">" + row[1] + "</a>";
            xtra += "</ul>";
            xtra += "</div>"

        xtra += self.get_layout("menu");
        xtra += "</div>"
        return xtra;

    def get_body(self):
        if (not self.page or self.page == ""):
            self.page = "top";

        if (self.search):
            return self.get_menu() + self.get_search();
        else:
            return self.get_menu() + self.get_page();
        
def errorpage(start_response,status,error):
    output=error;

    response_headers = [('Content-type', 'text/html'),
                        ('Content-Length', str(len(output)))]

    start_response(status, response_headers)
    return [output];

# this one will only be called on the web page
def application(environ, start_response):
    status = '200 OK'

    qs = parse_qs(environ['QUERY_STRING'])
    p = qs.get('page')
    s = qs.get('search')

    if error != False:
        return errorpage(start_response,status,error);

    if (p != None):
        p=p[0];
    elif(s != None):
        s=s[0];
    else:
        p="";

    page = Page(which=p,search=s);
    output  = "<!DOCTYPE html><html>";
    output += "<head>";
    output += page.get_head();
    output += "</head>";
    output += "<body>";
    output += page.get_layout("topbar");
    output += page.get_body();
    output += "</body>";
    output += "</html>";

    output = output.encode('utf-8');

    response_headers = [('Content-type', 'text/html'),
                        ('Content-Length', str(len(output)))]

    start_response(status, response_headers)
    return [output]

# and this one will only be called on the command line
if __name__ == '__main__':
    print ("Standalone version ------------")
    page = Page(which="",search="");
    output = page.get_body();
    print output
