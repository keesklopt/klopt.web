#!/usr/bin/python
import sys
import os
import socket
import urllib
import logging
# set the path to current dir for importing 
sys.path.append(os.path.dirname(__file__))

from cgi import parse_qs, escape

logger = logging.getLogger('webapp')
logger.setLevel(logging.DEBUG)

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

#formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s : %(message)s')
formatter = logging.Formatter('%(message)s')

ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)

# 'application' code example for logging, these will appear in 
# /var/log/apache2/error.log 
#logger.debug('debug message')
#logger.info('info message')
#logger.warn('warn message')
#logger.error('error message')
#logger.critical('critical message')

def errorpage(start_response,status,output):
    output.encode('utf-8')
    response_headers = [('content-type', 'text/html; charset=utf-8'),
                        ('content-length', str(len(output)+1)),
                        ('access-control-allow-origin', '*'),
                        ('access-control-allow-methods', '*'),
                        ('access-control-allow-headers', '*')]

    start_response(status, response_headers)
    return [output];

def exceptpage(start_response,status,error):
    output=""
    for i in range(0, 2):
        output += str(error[i]) + "\0"

    return errorpage(start_response,status,output)


# end-detect version of recv_all 
# buffers are terminated by '\0' so on each read test if the final character
# is the End Marker
End='\0'
def recv_end(the_socket):
    total_data=[];data=''
    while True:
        data=the_socket.recv(8192)
        l = len(data)
        logger.error(data)
        if data[l-1] == End:
            total_data.append(data[:l-1])
            break
        total_data.append(data)
    return ''.join(total_data)


def handle_server(query):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.connect(('localhost', 6660))
    except:
        query = "mislukt"

    #logger.error(query)
    
    # must include null as terminator
    # tryouts : \n did not work well (there is input with \n) MUST be \0
    # or maybe TLV solution (not tried). For now we do '\0' ...
    # it does not seem needed to encode the \0
    #query += '\0'.encode('ascii')
    query += '\0';
    s.sendall(query)

# THIS WILL FAIL for messages that are too big !!, since it may not 
# arrive in one piece !, fix this with prepending the size in 4 bytes!!
# maybe transfer this to tomcat as well ?
    try:
        data = recv_end(s);
    except socket.error as err:
        return str(err);

    logger.error (data);

    s.close();

    return data

def handle_get(qs):
    query = qs.get('jsonrpc',['']);
    return handle_server(query);

def handle_post(request_body):
    #request_body = urllib.parse.unquote(request_body)
    #logger.error(type(request_body))
    return handle_server(request_body);

# this one will only be called on the web page
def application(environ, start_response):
    status = '200 OK'
    output = 'unknown'

    try:
        env = environ['QUERY_STRING'];
        if (env):
            qs = parse_qs(env)
            if (qs):
                output = handle_get(qs)
        else:
            env = environ.get('CONTENT_LENGTH')
            l = int(env)
            if (l > 0):
                if (l):
                    inp = environ['wsgi.input'].read(l)
                    output = handle_post(inp)
                else:
                    output = "empty"
            else:
                return errorpage(start_response,status,"NO GET or POST");
                
    except: 
        return exceptpage(start_response,status,sys.exc_info());

    #output = str(environ)

    output.decode('utf-8')
    response_headers = [('content-type', 'text/html; charset=utf-8'),
                        ('content-length', str(len(output)+1)),
                        ('access-control-allow-origin', '*'),
                        ('access-control-allow-methods', '*'),
                        ('access-control-allow-headers', '*')]

    start_response(status, response_headers)

    return [output.strip('\0')];

# and this one will only be called on the command line
if __name__ == '__main__':
    print ("Standalone test page ------------")
    #print handle_server('{"jsonrpc":"2.0","method":"ping"}');
