<?

// this is a generic jsonrpc handler file 
// NOTE : it is adapted from adaptime's adap.php, but that did NOT use
// jsonrpc, but a self invented json-rpc using cmd instead of method etc
// that is why this file is explicitely called jsonrpc.php
session_start();

require_once "ajax.php";
require_once 'PHPExcel/PHPExcel/IOFactory.php';
include_once('opentbs/demo/tbs_class.php');
include_once('opentbs/tbs_plugin_opentbs.php');

$jsonrpc=$_GET['jsonrpc'];
if (!$jsonrpc) {
// askbar uses POST
// feyenoord should also ?
$jsonrpc=$_POST['jsonrpc'];
}

function json_error($inmsg,$err)
{
    $gm = new JsonRpcResponse($inmsg->id, NULL, $err);
    return $gm;
}

function get_dir($dir)
{
	$files = scandir($dir);

	return $files;
}

function make_csv($sheetdata)
{
	$res= "";
	for ($r=0;$r<count($sheetdata);$r++) {
		if ($r>0) $res .="\n";
		$subarr=$sheetdata[$r];
		$c=0;
		foreach ($subarr as $val) {
			if ($c>0) $res .=",";
			$res .= $val;
			$c++;
		} 
	} 

	return $res;
}

function del_file($dir,$fname)
{
	$fname = $dir . "/" . $fname;
	return unlink($fname);
}

function get_file($dir,$fname)
{
	$fname = $dir . "/" . $fname;
	$path_parts = pathinfo($fname);

	$inputFileType = 'Excel2007';
	$ext = $path_parts['extension'];

	if ($ext == "xls" || $ext == "xlsx" ) {

		if ($ext == "xls") $inputFileType = 'Excel5';

		$objReader = PHPExcel_IOFactory::createReader($inputFileType);
		$objPHPExcel = $objReader->load($fname);
		$sheet = $objPHPExcel->getActiveSheet();
		//$sheetData = $sheet->toArray();

		$maxCell = $sheet->getHighestRowAndColumn();
		$sheetData = $sheet->rangeToArray('A1:' . $maxCell['column'] . $maxCell['row']);
		$content = make_csv($sheetData);
	} else 
	if ($path_parts['extension'] == "odt") {
		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$TBS->LoadTemplate($fname);
		echo "loaded";

		//$content = make_csv($sheetData);
	} else {  
		$content = file_get_contents($fname);
	} 

	return $content;
}

function geocode($pc,$num,$street,$city,$country)
{
	
}

function json($data)
{
	$qry =$data . "\n";
	
	if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0)))
	{
		return false;
	}
	if(!socket_connect($sock , 'localhost' , 6660))
	{
		return false;
	}

	if( ! socket_send ( $sock , $qry , strlen($qry) , 0))
	{
		return false;
	}
 
	// just read a LOT
	$buf = socket_read($sock, 100000000, PHP_NORMAL_READ);
	//if ($buf != "pong");
	return $buf;
}

function test_server($inmsg)
{
    $which = $inmsg->get_param(0);
	$res= get_dir($which);

    $gm = new JsonRpcResponse($inmsg->id,$res,null);

    return $gm;
}

function list_files($inmsg)
{
    $which = $inmsg->get_param(0);
	$res= get_dir($which);

    $gm = new JsonRpcResponse($inmsg->id,$res,null);

    return $gm;
}

function read_file($inmsg)
{
    $dir = $inmsg->get_param(0);
    $which = $inmsg->get_param(1);
	$res= get_file($dir,$which);

    $gm = new JsonRpcResponse($inmsg->id,$res,null);

    return $gm;
}

function delete_file($inmsg)
{
    $dir = $inmsg->get_param(0);
    $which = $inmsg->get_param(1);
    $res= del_file($dir,$which);

    $gm = new JsonRpcResponse($inmsg->id,$res,null);

    return $gm;
}

function geo_code($id,$inmsg)
{
	$res= json($inmsg);
    $gm = new JsonRpcResponse($id,$res,null);

    return $gm;
}

function randomnodes($id,$inmsg)
{
	$res= json($inmsg);
    $gm = new JsonRpcResponse($id,$res,null);

    return $gm;
}

function transparent($inmsg)
{
	$res= json($inmsg);

    $gm = new JsonRpcResponse(11,$res,null);

    return $gm;
}


class returnobj
{
    function returnobj($x,$y) 
    {
        $this->x =$x;
        $this->y =$y;
    }
}

if ($jsonrpc) {
    $jm = new JsonRpcRequest($jsonrpc);

    // dispatch on cmd
    switch($jm->method) {
        case "test_server":
            $e=test_server($jm);
        break;
        case "list_files":
            $e=list_files($jm);
        break;
        case "randomnodes":
            $e=randomnodes($jm->id,$jsonrpc);
        break;
        case "geocode":
            $e=geo_code($jm->id,$jsonrpc);
        break;
        case "read_file":
            $e=read_file($jm);
        break;
        case "delete_file":
            $e=delete_file($jm);
        break;
        default:
			//print_r($jsonrpc);
            //echo "unknown jsonrpc command:".$jm->method."|";
			$e=transparent($jm);
        break;
    }
    $e->put();
} else {
    // AjaxContext examples
    switch ($method) {
        default:
            //echo "ask only uses Json messages";
		//print("Not recognized");
       print_r($_POST);
        break;
    }
}

?>
