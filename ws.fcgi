<IfModule mod_fastcgi.c>
    AddHandler fastcgi-script .fcgi .fcg
    FastCgiServer /var/www/html/web/ws -idle-timeout 1000 -processes 1
    ScriptAlias /ws "/var/www/html/web/ws"
    ScriptAlias /mat "/var/www/html/web/ws"
</IfModule>
